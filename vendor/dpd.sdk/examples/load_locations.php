<?php
require __DIR__ .'/../src/autoload.php';

$config = new \Ipol\DPD\Config\Config([
    'KLIENT_NUMBER'   => '1001027795',
    'KLIENT_KEY'      => '182A17BD6FC5557D1FCA30FA1D56593EB21AEF88',
    'KLIENT_CURRENCY' => 'RUB',
]);
$table  = \Ipol\DPD\DB\Connection::getInstance($config)->getTable('location');
$api    = \Ipol\DPD\API\User\User::getInstanceByConfig($config);

$loader = new \Ipol\DPD\DB\Location\Agent($api, $table);
$loader->loadAll();
$loader->loadCashPay();
