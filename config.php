<?php

return [
    /** Данные для подключения к БД (опционально, по умолчанию используется локальный SQLite) */
    // 'DB' => [
    //     'DSN'      => '',
    //     'USERNAME' => '',
    //     'PASSWORD' => '',
    // ],

    /** Данные авторизации */
    'KLIENT_NUMBER'      => '',
    'KLIENT_KEY'         => '',
    'KLIENT_CURRENCY'    => 'RUB',

    /** Тестовые режим */
    'IS_TEST'            => false,

    /** Габариты по умолчанию (указываются в граммах и миллиметрах) */
    'WEIGHT' => 1000,
    'LENGTH' => 200,
    'WIDTH'  => 100,
    'HEIGHT' => 200,

    /** Тарифы исключенные из расчетов */
    'TARIFF_OFF' => [
        // "PCL", // DPD Online Classic
        // "CSM", // DPD Online Express
        // "ECN", // DPD ECONOMY
        // "ECU", // DPD ECONOMY CU
    ],
];
